package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static
     * fields.
     * This field is a logger. Loggers are like a more advanced println, for writing
     * messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does
     * something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testBoxIsIncomplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 4, 2);
        int x = 0, y = 0, player = 1;

        grid.drawHorizontal(x, y, player);
        grid.drawHorizontal(x + 1, y, player);
        grid.drawVertical(x, y, player);

        assertFalse(grid.boxComplete(x, y), "Box not fully drawn!");

    }

    @Test
    public void testForAlreadyDrawnHorizontalLine() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 5, 2);
        int x = 0, y = 0, player = 1;

        grid.drawHorizontal(x, y, player);
        grid.drawHorizontal(x + 1, y, player);
        grid.drawVertical(x, y, player);

        assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(x, y, player);
        });
    }

    @Test
    public void testForAlreadyDrawnVerticalLine() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 5, 2);
        int x = 0, y = 0, player = 1;

        grid.drawHorizontal(x, y, player);
        grid.drawHorizontal(x + 1, y, player);
        grid.drawVertical(x, y, player);

        assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(x, y, player);
        });
    }
}